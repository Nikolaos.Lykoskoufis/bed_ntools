# BED_ntools
## Created by Nikos Lykoskoufis. 

This suit of functions are not meant to replace BEDtools. They are just some useful functions for filtering samples and phenotypes from a bed file. 
