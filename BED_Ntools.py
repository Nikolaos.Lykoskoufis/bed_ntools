#!/usr/bin/env python3 

import sys 
import pandas as pd 
import argparse 
import gzip

def myopen(filename):
    '''Checking to see if file is gzipped or not'''
    f = gzip.open(filename)
    try:
        f.read(2)
        f.close()
        return gzip.open(filename,"rt")
    except:
        f.close()
        return open(filename,"rt")

def samples_to_exclude_list_generator(filename) :
    '''This function reads the file containing the samples to exclude and creates a list. Filename is a list of samples to remove from the bed file.'''
    samples_list = []
    with open(filename, "rt") as f :
        for line in (line.rstrip().split() for line in f) :
            samples_list.append("".join(line))
    
    return samples_list

def Bed_sample_filter(BED,samples_to_filter, out) :
        '''This script reads a bed file and a file containing samples to be excluded and output the results in a new output.
        The file containing the samples to exclude should contain one sample per line.'''
        samples_to_exclude = samples_to_exclude_list_generator(samples_to_filter)
        #print(samples_to_exclude)
        bed_file = pd.read_table(BED, sep="\t", header=0, delim_whitespace= False)
        for sample in samples_to_exclude :
            bed_file.drop(sample,1,inplace=True)
            bed_file.to_csv(out,sep="\t")

#Bed_sample_filter(*sys.argv[1:])

def Bed_samples_keep(BED, samples_to_keep, out) :
    '''This script reads a bed file and a file containing samples to be kept and output the results in a new output.
        The file containing the samples to keep should contain one sample per line.'''
    samples_to_keep= samples_to_exclude_list_generator(samples_to_keep)
    samples_to_keep = ["#chr","start","end","gene","info","strand"] + samples_to_keep
    bed_file = pd.read_table(BED, sep="\t", header=0, delim_whitespace= False,dtype=str)
    bed_file = bed_file[samples_to_keep]
    bed_file.to_csv(out, sep="\t", index=False)


def Bed_filter_phenotypes(BED,phenotypes_to_include, out) : 
    '''This function reads a bed file and only includes to the output file the phenotype types specified'''
    f = myopen(BED)
    g = open(out, "w")
    g.write(f.readline())
    for line in (line.rstrip().split() for line in f) :
        info = line[4]
        if info.split(";")[1].split("=")[0] != "T" :
            print("Error. The second element in the info is not phenotype type. PLease check your bedfile. The info column should contain the following information\nL=\{length\};T=\{type\};R=\{region\};N=\{name of phenotype\}")
        else :
            type = info.split(";")[1].split("=")[1]
            if type not in phenotypes_to_include : 
                continue 
            else :
                g.write("\t".join(line)+"\n")

def extract_region(BED,region) :
    '''This function reads a bed file and extracts from it a genomic region. It returns all records into a list. This function is only usefull when needed inside a script. 
    From command line you just need to sort, bgzip and tabix -p bed the bed filea and then use the following command to extract the required region : [tabix <bed file> <region>]'''
    url = BED
    tb = tabix.open(url)
    records = tb.querys(region)
    return records

def Bed_filter_missing(BED,perc_missing,out) :
    '''This function reads a bed file and computes the percentage of missing data and filters based on the threshold given''' 
    f = myopen(BED)
    g = open(out, "w")
    g.write(f.readline())
    for line in (line.rstrip().split() for line in f) :
        exp = line[6:]
        exp = [float(i) for i in exp]
        length = len(exp)
        missing = [float(i) for i in exp if i == 0]
        if ((len(missing) / length)*100) >= float(perc_missing) :
            continue 
        else :
            g.write("\t".join(line)+"\n")

def Bed_filter_present(BED,perc_present,out) :
    '''This function reads a bed file and keeps only phenotypes that contain X% of non-zero data.''' 
    f = myopen(BED)
    g = open(out, "w")
    g.write(f.readline())
    for line in (line.rstrip().split() for line in f) :
        exp = line[6:]
        exp = [float(i) for i in exp]
        length = len(exp)
        present = [float(i) for i in exp if i != 0]
        if ((len(present) / length)*100) <= float(perc_present) :
            continue 
        else :
            g.write("\t".join(line)+"\n")


############################################
#               PARSER                     #
############################################


parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description='Tool for filtering columns on bedfiles.')

subparsers = parser.add_subparsers(help="",dest="cmd",description="Available modes")

fp_parser=subparsers.add_parser("exclude",help="Which sample names you want to remove from bedfile.")
mm_parser=subparsers.add_parser("include",help="Which sample names you want to keep in your bedfile. (same as remove but instead of giving a list of samples to remove you pass a list of samples to keep)" )
ph_parser=subparsers.add_parser("phen_include",help="Which phenotype type to keep in your bedfile.")
bh_parser=subparsers.add_parser("filter_missing",help="Filter for missing data based on threshold.")
ah_parser=subparsers.add_parser("filter_present",help="Keep only percentage of data that is non-zero based on threshold.")

fp_IO = fp_parser.add_argument_group('Main arguments')
fp_optional = fp_parser.add_argument_group('optional arguments')
fp_IO.add_argument('-bed', '--bed', dest='BED',nargs=1, type=str, help="Absolute path for the BEDfile to filter.")
fp_IO.add_argument('-include', '--samples-to-exclude', dest='samples_to_exclude', type=str, nargs=1, help='Absolute path fastq to the file containing the samples to exclude.')
fp_IO.add_argument('-out','--output', dest="output", type=str, nargs=1, help="The output file to write results")


mm_IO = mm_parser.add_argument_group('Main arguments')
mm_IO.add_argument('-bed', '--bed', dest='BED',nargs=1, type=str, help="Absolute path for the BEDfile to filter.")
mm_IO.add_argument('-include', '--samples-to-include', dest='samples_to_include', type=str, nargs=1, help='Absolute path fastq to the file containing the samples to include.')
mm_IO.add_argument('-out','--output', dest="output", type=str, nargs=1, help="The output file to write results")


ph_IO = ph_parser.add_argument_group('Main arguments')
ph_optional = ph_parser.add_argument_group('optional arguments')
ph_IO.add_argument('-bed', '--bed', dest='BED',nargs=1, type=str, help="Absolute path for the BEDfile to filter.")
ph_IO.add_argument('-include', '--phen-to-include', dest='phenotypes_to_include', type=str, nargs='+', help='list of phenotype type to keep. You can choose between these ones. [\'3prime_overlapping_ncrna\',\'antisense\',\'IG_C_gene\',\'IG_C_pseudogene\',\'IG_D_gene\',\'IG_J_gene\',\'IG_J_pseudogene\',\'IG_V_gene\',\'IG_V_pseudogene\',\'info\',\'lincRNA\',\'miRNA\',\'misc_RNA\',\'polymorphic_pseudogene\',\'processed_transcript\',\'protein_coding\',\'pseudogene\',\'rRNA\',\'sense_intronic\',\'sense_overlapping\',\'snoRNA\',\'snRNA\',\'TR_C_gene\',\'TR_J_gene\',\'TR_V_gene\',\'TR_V_pseudogene\']')
ph_IO.add_argument('-out','--output', dest="output", type=str, nargs=1, help="The output file to write results")


bh_IO = bh_parser.add_argument_group('Main arguments')
bh_optional = bh_parser.add_argument_group('optional arguments')
bh_IO.add_argument('-bed', '--bed', dest='BED',nargs=1, type=str, help="Absolute path for the BEDfile to filter.")
bh_IO.add_argument('-perc', '--percentage', dest='perc_missing', type=str, nargs=1, help='Percentage of missing data to keep. If missing data is higher or equal to threshold, then filter out data.')
bh_IO.add_argument('-out','--output', dest="output", type=str, nargs=1, help="The output file to write results")


ah_IO = ah_parser.add_argument_group('Main arguments')
ah_optional = ah_parser.add_argument_group('optional arguments')
ah_IO.add_argument('-bed', '--bed', dest='BED',nargs=1, type=str, help="Absolute path for the BEDfile to filter.")
ah_IO.add_argument('-perc', '--percentage', dest='perc_present', type=str, nargs=1, help='Percentage of present data to keep. If present data is higher or equal to threshold, then keep phenotypes.')
ah_IO.add_argument('-out','--output', dest="output", type=str, nargs=1, help="The output file to write results")





args = parser.parse_args()
if __name__ == '__main__' :
    if args.cmd == "exclude" :
        if args.BED == None and args.samples_to_exclude == None and args.output == None :
            print("ERROR! Some arguments are missing!")
            sys.exit(1)
        else : 
            Bed_sample_filter(args.BED,args.samples_to_exclude, args.output)
    if args.cmd == "include" :
        if args.BED == None and args.samples_to_include == None and args.output == None :
            print("ERROR! Some arguments are missing!")
            sys.exit(1)
        else : 
            Bed_samples_keep(args.BED[0],args.samples_to_include[0], args.output[0])
    if args.cmd == "phen_include" :
        if args.BED == None and args.phenotypes_to_include == None and args.output == None :
            print("ERROR! Some arguments are missing!")
            sys.exit(1)
        else : 
            #print(args)
            Bed_filter_phenotypes(args.BED[0],args.phenotypes_to_include, args.output[0])
    if args.cmd == "filter_missing" :
        if args.BED == None and args.perc_missing == None and args.output == None : 
            print("Error! Some arguments are missing!")
            sys.exit(1)
        else : 
            Bed_filter_missing(args.BED[0],args.perc_missing[0],args.output[0])
    if args.cmd == "filter_present" :
        if args.BED == None and args.perc_present == None and args.output == None : 
            print("Error! Some arguments are missing!")
            sys.exit(1)
        else : 
            Bed_filter_missing(args.BED[0],args.perc_present[0],args.output[0])